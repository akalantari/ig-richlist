<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('influencers', 'App\Http\Controllers\InfluencersController@index');
Route::get('influencers/{id}', 'App\Http\Controllers\InfluencersController@show');
Route::post('influencers', 'App\Http\Controllers\InfluencersController@store');
Route::put('influencers/{id}', 'App\Http\Controllers\InfluencersController@update');
Route::delete('influencers/{id}', 'App\Http\Controllers\InfluencersController@destroy');
