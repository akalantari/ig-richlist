# Run and Deploy
* First, run `composer install` to install required dependencies
* Use `docker-compose up -d` to build docker images and run containers
* Run `./vendor/bin/sail artisan migrate` to setup DB
* Run `./vendor/bin/sail artisan db:seed` to populate DB 

# Usage
Use Postman or similar structures to connect to endpoints, use `Content-Type: application/json` header when using `POST` and `PUT`
## Endpoints
- `GET /influencers` To get list of influencers
- `POST /influencers` To create a new record
- `PUT /influencers/{slug}` To update an existing record
- `GET /influencers/{slug}` To fetch an existing record
- `DELETE /influencers/{slug}` To delete a record

### POST and PUT accepted inputs
```
[
    "name": string,
    "username": string,
    "country_code": string,
    "category_code": string,
    "followers": integer,
    "cost_per_post": integer,
    "rank": integer
]
```

## Tests
Run `./vendor/bin/sail artisan test` to start testing
