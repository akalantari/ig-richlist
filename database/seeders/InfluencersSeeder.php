<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class InfluencersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Read the init-list values
        // Setup file path
        $initValDirPath = realpath(dirname(__FILE__)) . '/initial-values/';
        $initValFilePath = $initValDirPath . 'instagram-rich-list.json';
        $initValRaw = file_get_contents($initValFilePath); // read file
        $initValCelebrities = json_decode($initValRaw); // Parse to JSON

        // Prepare for iteration
        $countries = []; // Making in-scope cache
        $categories = []; // Making in-scope cache

        // Iterator over init values
        foreach( $initValCelebrities as $influencer )
        {
            if( empty($countries[$influencer->country_code]) ) // If not queried before, get and cache the result
            {
                $countries[ $influencer->country_code ] = Country::getByCode( $influencer->country_code );
            }

            if( empty($categories[ $influencer->category_code ]) ) // If not queried before, get and cache the result
            {
                $categories[ $influencer->category_code ] = Category::getByCode( $influencer->category_code );
            }

            \DB::table('influencers')->insert([
                'name' => $influencer->name,
                'slug' => $influencer->slug,
                'username' => $influencer->username,
                'category_id' => $categories[ $influencer->category_code ]->id,
                'country_id' => $countries[ $influencer->country_code ]->id,
                'followers' => $influencer->followers,
                'followers' => $influencer->followers,
                'cost_per_post' => $influencer->cost_per_post,
                'rank' => $influencer->rank,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

        }

    }
}
