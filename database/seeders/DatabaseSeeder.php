<?php

namespace Database\Seeders;

use Celebrities;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategoriesSeeder::class,
            CountriesSeeder::class,
            InfluencersSeeder::class
        ]);
    }
}
