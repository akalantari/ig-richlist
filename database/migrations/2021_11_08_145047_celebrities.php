<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Celebrities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Celebrities DB tables

        // Categories Table
        Schema::create('categories', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->string('code')->unique();
            $table->timestamps();
        });

        // Countries Table
        Schema::create('countries', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->string('code')->unique();
            $table->timestamps();
        });

        // Celebrities Table
        Schema::create('influencers', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('username')->unique();
            $table->bigInteger('category_id', false, true)->index();
            $table->bigInteger('country_id', false, true)->index();
            $table->integer('followers');
            $table->integer('cost_per_post');
            $table->integer('rank')->index();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('restrict');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('celebrities');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('categories');
    }
}
