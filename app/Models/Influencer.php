<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Influencer extends Model
{
    use HasFactory;

    protected $table = 'influencers';
    protected $hidden = ['id', 'created_at', 'updated_at', 'category_id', 'country_id'];
    protected $appends = ['country_code', 'country_name', 'category_code', 'category_name'];

    protected $fillable = [
        'name',
        'slug',
        'username', // This is the IG username of the celebrity
        'country_id',
        'country_code',
        'category_id',
        'category_code',
        'followers',
        'cost_per_post',
        'rank'
    ];

    /**
     * @param string $slug Requested celebrity slug
     * @return Influencer|null
     */
    public static function getBySlug(string $slug): ?Influencer
    {
        return Influencer::where('slug','=',$slug)->first();
    }

    /**
     * The country of the celebrity.
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id')->first();
    }

    /**
     * The category of the celebrity.
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->first();
    }

    /**
     * Set the value of slug automatically based on the given name
     */
    public function setNameAttribute($name)
    {
        $this->attributes['slug'] =  Str::slug($name);
        $this->attributes['name'] =  $name;
    }

    public function getCountryCodeAttribute()
    {
        return $this->attributes['country_code'] = $this->country()->code;
    }

    /**
     * Convert country code to id
     */
    public function setCountryCodeAttribute($countryCode)
    {
        $this->attributes['country_id'] =  Country::getByCode($countryCode)->id;
    }

    public function getCountryNameAttribute()
    {
        return $this->attributes['country_name'] = $this->country()->name;
    }

    public function getCategoryCodeAttribute()
    {
        return $this->attributes['category_code'] = $this->category()->code;
    }

    /**
     * Convert category code to id
     */
    public function setCategoryCodeAttribute($categoryCode)
    {
        $this->attributes['category_id'] =  Category::getByCode($categoryCode)->id;
    }

    public function getCategoryNameAttribute()
    {
        return $this->attributes['category_name'] = $this->category()->name;
    }
}
