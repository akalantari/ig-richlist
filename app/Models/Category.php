<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'code'
    ];

    /**
     * @param string $code Pass the requested category code
     * @return App\Models\Category|null
     */
    public static function getByCode(string $code): ?Category
    {
        return Category::where('code','=',$code)->first();
    }

}
