<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table = 'countries';

    protected $fillable = [
        'name',
        'code'
    ];

    /**
     * @param string $code Pass the requested country code
     * @return App\Models\Country|null
     */
    public static function getByCode(string $code): ?Country
    {
        return Country::where('code','=',$code)->first();
    }
}
