<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Influencer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InfluencersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Influencer::query()->orderBy('rank', 'ASC');

        if( $request->query('category') ) { // if category filter is being applied
            $categoryCode = $request->query('category');
            $category = Category::getByCode($categoryCode); // Get the category
            if( !$category ) { // INVALID_CATEGORY error
                return response()->json([
                    'success' => false,
                    'error' => 'INVALID_CATEGORY',
                    'message' => 'Passed Category Code Is Invalid'
                ], 400);
            }

            $query->where('category_id','=',$category->id); // apply the filter on the query
        }

        if( $request->query('country') ) { // if country filter is being applied
            $countryCode = $request->query('country');
            $country = Country::getByCode($countryCode); // Get the category
            if( !$country ) { // INVALID_COUNTRY error
                return response()->json([
                    'success' => false,
                    'error' => 'INVALID_COUNTRY',
                    'message' => 'Passed Country Code Is Invalid'
                ], 400);
            }

            $query->where('country_id','=',$country->id); // apply the filter on the query
        }

        return response()->json([
            'success' => true,
            'response' => $query->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'success' => true,
        ];

        // Preparing validation rules
        $validation = Validator::make($request->all(),[
            'username' => 'required|string|max:128',
            'name' => 'required|string|max:128',
            'country_code' => 'required|exists:countries,code',
            'category_code' => 'required|exists:categories,code',
            'followers' => 'required|integer',
            'rank' => 'required|integer',
            'cost_per_post' => 'required|integer',
        ]);

        if( $validation->fails() ) {
            $response['success'] = false;
            $response['error'] = 'INVALID_DATA';
            $response['message'] = $validation->getMessageBag();
            return response()->json($response, 400);
        }

        // accepted inputs
        // Slug is not a valid input, it will be automatically assigned
        $acceptedInputs = [
            'username',
            'name',
            'country_code',
            'category_code',
            'followers',
            'rank',
            'cost_per_post'
        ];

        $values = $request->only($acceptedInputs);

        $influencer = new Influencer($values);
        $influencer->save();

        $response['response'] = $influencer;

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $influencer = Influencer::getBySlug($slug);

        $response = [
            'success' => !!$influencer, // true if only user found
        ];
        $statusCode = 200;

        if( !$influencer ) {
            $response['error'] = 'INFLUENCER_NOT_FOUND';
            $response['message'] = 'The requested Influencer was not found';
            $statusCode = 404;
        } else {
            $response['response'] = $influencer;
        }

        return response()->json($response, $statusCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $influencer = Influencer::getBySlug($slug);

        $response = [
            'success' => !!$influencer, // true if only user found
        ];
        $statusCode = 200;

        if( !$influencer ) {
            $response['error'] = 'INFLUENCER_NOT_FOUND';
            $response['message'] = 'The requested Influencer was not found';
            $statusCode = 404;
            return response()->json($response, $statusCode);
        }

        // Preparing validation rules
        $validation = Validator::make($request->all(),[
            'username' => 'string|max:128',
            'name' => 'string|max:128',
            'country_code' => 'exists:countries,code',
            'category_code' => 'exists:categories,code',
            'followers' => 'integer',
            'rank' => 'integer',
            'cost_per_post	' => 'integer',
        ]);

        if( $validation->fails() ) {
            $response['success'] = false;
            $response['error'] = 'INVALID_DATA';
            $response['message'] = $validation->getMessageBag();
            return response()->json($response, 400);
        }

        // accepted inputs
        // Slug is not a valid input, it will be automatically assigned
        $acceptedInputs = [
            'username',
            'name',
            'followers',
            'country_code',
            'category_code',
            'rank',
            'cost_per_post'
        ];
        $values = $request->only($acceptedInputs);

        $influencer->update($values);
        $response['response'] = $influencer;

        return response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $influencer = Influencer::getBySlug($slug);

        $response = [
            'success' => !!$influencer, // true if only user found
        ];
        $statusCode = 200;

        if( !$influencer ) {
            $response['error'] = 'INFLUENCER_NOT_FOUND';
            $response['message'] = 'The requested Influencer was not found';
            $statusCode = 404;
            return response()->json($response, $statusCode);
        }

        $influencer->delete();

        return response()->json($response, $statusCode);
    }
}
