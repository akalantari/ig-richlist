<?php

namespace Tests\Unit;

use App\Models\Country;
use App\Models\Category;
use App\Models\Influencer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InfluencerTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_create_influencer()
    {
        $testModel = new Influencer([
            'name' => 'Ali Kalantari',
            'username' => 'akalantari',
            'category_code' => 'influencer',
            'country_code' => 'asia',
            'followers' => 1,
            'cost_per_post' => 10,
            'rank' => 100
        ]);

        $this->assertTrue($testModel->save());
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_influencer()
    {
        $testModel = Influencer::getBySlug('ali-kalantari');
        $this->assertInstanceOf(Influencer::class, $testModel);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_update_influencer()
    {
        $testModel = Influencer::getBySlug('ali-kalantari');
        $testModel->name = 'Ali Kalantari Taft';
        $this->assertTrue($testModel->save());
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_influencer_country()
    {
        $testModel = Influencer::getBySlug('ali-kalantari-taft');
        $this->assertInstanceOf(Country::class, $testModel->country());
        $this->assertEquals('asia', $testModel->country()->code);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_influencer_category()
    {
        $testModel = Influencer::getBySlug('ali-kalantari-taft');
        $this->assertInstanceOf(Category::class, $testModel->category());
        $this->assertEquals('influencer', $testModel->category()->code);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_delete_influencer()
    {
        $testModel = Influencer::getBySlug('ali-kalantari-taft');
        $this->assertTrue($testModel->delete());
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_influencer_unsuccessful()
    {
        $testModel = Influencer::getBySlug('ali-kalantari-taft');
        $this->assertNull($testModel);
    }
}
