<?php

namespace Tests\Unit;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_category()
    {
        $testModel = Category::getByCode('sport');
        $this->assertInstanceOf(Category::class, $testModel);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_category_unsuccessful()
    {
        $testModel = Category::getByCode('sports');
        $this->assertNull($testModel);
    }
}
