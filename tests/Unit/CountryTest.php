<?php

namespace Tests\Unit;

use App\Models\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CountryTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_country()
    {
        $testModel = Country::getByCode('europe');
        $this->assertInstanceOf(Country::class, $testModel);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_country_unsuccessful()
    {
        $testModel = Country::getByCode('iran');
        $this->assertNull($testModel);
    }
}
