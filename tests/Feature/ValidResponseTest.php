<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ValidResponseTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_index_response()
    {
        $response = $this->get('/influencers');

        $response->assertStatus(200)
            ->assertJson(
                [
                    'success' => true,
                    'response' => [
                        [
                            'country_code' => true,
                            'country_name' => true,
                            'category_code' => true,
                            'category_name' => true,
                            'username' => true,
                            'name' => true,
                            'slug' => true,
                            'followers' => true,
                            'cost_per_post' => true,
                            'rank' => true,
                        ]
                    ]
                ]
            );
    }

    public function test_influencer_create()
    {
        $response = $this->post('/influencers', [
            'name' => 'Ali',
            'username' => 'akalantari',
            'country_code' => 'asia',
            'category_code' => 'influencer',
            'followers' => 10,
            'rank' => 1000,
            'cost_per_post' => 10
        ]);

        $response->assertStatus(200)
            ->assertJson(
                [
                    'success' => true,
                    'response' => [
                        'name' => 'Ali',
                        'username' => 'akalantari',
                        'slug' => 'ali',
                        'country_code' => 'asia',
                        'country_name' => 'Asia',
                        'category_code' => 'influencer',
                        'category_name' => 'Influencer',
                        'followers' => 10,
                        'rank' => 1000,
                        'cost_per_post' => 10
                    ]
                ]
            , true);
    }

    public function test_influencer_update()
    {
        $response = $this->put('/influencers/ali', [
            'name' => 'Ali 2',
            'username' => 'akalantari-2',
            'country_code' => 'europe',
            'category_code' => 'sport',
            'followers' => 100,
            'rank' => 10000,
            'cost_per_post' => 100
        ]);

        $response->assertStatus(200)
            ->assertJson(
                [
                    'success' => true,
                    'response' => [
                        'name' => 'Ali 2',
                        'username' => 'akalantari-2',
                        'slug' => 'ali-2',
                        'country_code' => 'europe',
                        'country_name' => 'Europe',
                        'category_code' => 'sport',
                        'category_name' => 'Sport',
                        'followers' => 100,
                        'rank' => 10000,
                        'cost_per_post' => 100
                    ]
                ]
            , true);
    }

    public function test_fetch_influencer()
    {
        $response = $this->get('/influencers/ali-2');

        $response->assertStatus(200)
            ->assertJson(
                [
                    'success' => true,
                    'response' => [
                        'name' => 'Ali 2',
                        'username' => 'akalantari-2',
                        'slug' => 'ali-2',
                        'country_code' => 'europe',
                        'country_name' => 'Europe',
                        'category_code' => 'sport',
                        'category_name' => 'Sport',
                        'followers' => 100,
                        'rank' => 10000,
                        'cost_per_post' => 100
                    ]
                ]
            , true);
    }

    public function test_delete_influencer()
    {
        $response = $this->delete('/influencers/ali-2');

        $response->assertStatus(200)
            ->assertJson(
                [
                    'success' => true
                ]
            , true);
    }

    public function test_single_value_not_found()
    {
        $response = $this->get('/influencers/ali-2');

        $response->assertStatus(404)
            ->assertJson(
                [
                    'success' => false,
                    'error' => 'INFLUENCER_NOT_FOUND',
                    'message' => 'The requested Influencer was not found'
                ]
            , true);
    }
}
